package com.domikoapp.jdbc.sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

public class JDBCSample {

	private final static String DB_URL = "jdbc:mysql://10.7.10.90:3306/y_omido";
	private final static String DB_USER = "y_omido";
	private final static String DB_PASSWORD = "y_omido";

	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
	}

	public void select() {
		try (Connection connection = this.getConnection()) {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from users");
			while (rs.next()) {
				System.out.println("id   = " + rs.getInt("id"));
				System.out.println("name = " + rs.getString("name"));
				System.out.println("age  = " + rs.getInt("age"));
				System.out.println("=====================");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insert(Integer id, String name, Optional<Integer> age) {
		try (Connection connection = this.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("insert into users values (?, ?, ?)");
			statement.setInt(1, id);
			statement.setString(2, name);
			statement.setInt(3, age.orElse(null));
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		JDBCSample sample = new JDBCSample();
		sample.insert(4, "Sample User", Optional.of(25));
		sample.select();
	}
}
